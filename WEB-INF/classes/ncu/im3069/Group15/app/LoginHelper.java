package ncu.im3069.Group15.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;

import ncu.im3069.Group15.util.DBMgr;




public class LoginHelper {
	
	  
	    private LoginHelper() {
	        
	    }
	    
	  
	    private static LoginHelper lh;
	    
	  
	    private Connection conn = null;
	    
	   
	    private PreparedStatement pres = null;

	    public static LoginHelper getHelper() {
	     
	        if(lh == null) lh = new LoginHelper();
	        
	        return lh;
	    }
	
	
	    public int getByData(Login l) {
       
        //Login l = null;
      
        JSONArray jsa = new JSONArray();
      
        String exexcute_sql = "";
       
        long start_time = System.nanoTime();
       
        int row = -1;
        
        ResultSet rs = null;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            String sql = "SELECT count(*) FROM `missa`.`members` WHERE `email` = ? and `password` = ? " ;
            
            
            String email = l.getEmail();
            String password = l.getPassword();
            
            /** 將參數回填至SQL指令當中 */
            pres = conn.prepareStatement(sql);
            pres.setString(1, email);
            pres.setString(2, password);
             /** 執行查詢之SQL指令並記錄其回傳之資料 */
            rs = pres.executeQuery();
            
            rs.next();
            row = rs.getInt("count(*)");
            System.out.print(row);
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(rs, pres, conn);
        }
        
        /** 
         * 判斷是否已經有資料
         * 若一筆則回傳 1，否則回傳 0 
         */
        
        if(row == 1) {
        	return 1;
        }else
        {
        return 0;
	}
	    }}
    
