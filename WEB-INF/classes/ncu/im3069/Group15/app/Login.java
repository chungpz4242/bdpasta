package ncu.im3069.Group15.app;

import org.json.JSONObject;

public class Login {
	
    private int id;
    
    
    private String email;
    
    
    private String name;
    
   
    private String password;
    
   
    private int login_times;
    
   
    private String status;
    
    private String phone;
    

    private LoginHelper lh =  LoginHelper.getHelper();
    
    //constructors
    public Login(int id, String email, String password, String name, int login_times, String status,String phone) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.login_times = login_times;
        this.status = status;
        this.phone = phone;
        
        
        }
    public Login(String email, String password) {
        this.email = email;
        this.password = password;
   
        
        
        }

    
    
    //function
    public JSONObject getData() {
    	 JSONObject jso = new JSONObject();
         jso.put("id", getId());
         jso.put("name", getName());
         jso.put("email", getEmail());
         jso.put("password", getPassword());
         jso.put("login_times", getLogin_times());
         jso.put("status", getStatus());
         jso.put("phone", getPhone());
         return jso;
    }


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public int getLogin_times() {
		return login_times;
	}


	public void setLogin_times(int login_times) {
		this.login_times = login_times;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public LoginHelper getLh() {
		return lh;
	}


	public void setLh(LoginHelper lh) {
		this.lh = lh;
	}
    
    
}

