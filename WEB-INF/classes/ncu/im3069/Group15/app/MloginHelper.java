package ncu.im3069.Group15.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;

import ncu.im3069.Group15.util.DBMgr;



public class MloginHelper {
	
	  private MloginHelper() {
	        
	    }
	    	  
	    private static MloginHelper mlh;
	    
	    /** 儲存JDBC資料庫連線 */
	    private Connection conn = null;
	    
	    /** 儲存JDBC預準備之SQL指令 */
	    private PreparedStatement pres = null;

	    public static MloginHelper getHelper() {
	        

	        if(mlh == null) mlh = new MloginHelper();
	        
	        return mlh;
	    }
	
	
	    public int getByData(Mlogin ml) {
     
      //Login l = null;
      /** 用於儲存所有檢索回之會員，以JSONArray方式儲存 */
      JSONArray jsa = new JSONArray();
      /** 記錄實際執行之SQL指令 */
      String exexcute_sql = "";
      /** 紀錄程式開始執行時間 */
      long start_time = System.nanoTime();
      /** 紀錄SQL總行數 */
      int row = -1;
      /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
      ResultSet rs = null;
      
      try {
          /** 取得資料庫之連線 */
          conn = DBMgr.getConnection();
          /** SQL指令 */
          String sql = "SELECT count(*) FROM `missa`.`manager` WHERE `email` = ? and `password` = ? " ;
          
          
          String email = ml.getEmail();
          String password = ml.getPassword();
          
          /** 將參數回填至SQL指令當中 */
          pres = conn.prepareStatement(sql);
          pres.setString(1, email);
          pres.setString(2, password);
           /** 執行查詢之SQL指令並記錄其回傳之資料 */
          rs = pres.executeQuery();
          
          rs.next();
          row = rs.getInt("count(*)");
          System.out.print(row);
          exexcute_sql = pres.toString();
          System.out.println(exexcute_sql);

      } catch (SQLException e) {
          /** 印出JDBC SQL指令錯誤 **/
          System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
      } catch (Exception e) {
          /** 若錯誤則印出錯誤訊息 */
          e.printStackTrace();
      } finally {
          /** 關閉連線並釋放所有資料庫相關之資源 **/
          DBMgr.close(rs, pres, conn);
      }
      
      /** 
       * 判斷是否已經有資料
       * 若一筆則回傳False，否則回傳True 
       */
      
      if(row == 1) {
      	return 1;
      }else
      {
      return 0;
	}
	    }

}
