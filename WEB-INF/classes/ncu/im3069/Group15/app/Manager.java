package ncu.im3069.Group15.app;

import org.json.*;
import java.util.Calendar;



public class  Manager {
    
 
    private int id;
    
   
    private String email;
    
  
    private String name;
    
   
    private String password;
    
   
    private int login_times;
    
   
    private String status;
    
    private String phone;
    
    private ManagerHelper mgh =  ManagerHelper.getHelper();
    
    public Manager(String email, String password, String name, String phone) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.phone = phone;
        update();
    }

    
    public Manager(int id, String email, String password, String name, String phone) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.phone= phone;
     
        getLoginTimesStatus();
    
        calcAccName();
    }
    
  
    public Manager(int id, String email, String password, String name, int login_times, String status,String phone) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.login_times = login_times;
        this.status = status;
        this.phone = phone;
    }
    
    
    public int getID() {
        return this.id;
    }

    
    public String getEmail() {
        return this.email;
    }
    
    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return this.password;
    }
    
    public int getLoginTimes() {
        return this.login_times;
    }
    
    
    public String getStatus() {
        return this.status;
    }
    
    public String getPhone() {
        return this.phone;
    }
    
   
    public JSONObject update() {
        /** 新建一個JSONObject用以儲存更新後之資料 */
        JSONObject data = new JSONObject();
        /** 取得更新資料時間（即現在之時間）之分鐘數 */
        Calendar calendar = Calendar.getInstance();
        this.login_times = calendar.get(Calendar.MINUTE);
        /** 計算帳戶所屬之組別 */
        calcAccName();
        
        /** 檢查該名會員是否已經在資料庫 */
        if(this.id != 0) {
            /** 若有則將目前更新後之資料更新至資料庫中 */
            mgh.updateLoginTimes(this);
            /** 透過ManagerHelper物件，更新目前之會員資料置資料庫中 */
            data = mgh.update(this);
        }
        
        return data;
    }
    
   
    public JSONObject getData() {
        /** 透過JSONObject將該名會員所需之資料全部進行封裝*/ 
        JSONObject jso = new JSONObject();
        jso.put("id", getID());
        jso.put("name", getName());
        jso.put("email", getEmail());
        jso.put("password", getPassword());
        jso.put("login_times", getLoginTimes());
        jso.put("status", getStatus());
        jso.put("phone", getPhone());
        return jso;
    }
    
    /**
     * 取得資料庫內之更新資料時間分鐘數與會員組別
     *
     */
    private void getLoginTimesStatus() {
        /** 透過ManagerHelper物件，取得儲存於資料庫的更新時間分鐘數與會員組別 */
        JSONObject data = mgh.getLoginTimesStatus(this);
        /** 將資料庫所儲存該名會員之相關資料指派至Member物件之屬性 */
        this.login_times = data.getInt("login_times");
        this.status = data.getString("status");
    }
    
    /**
     * 計算會員之組別<br>
     * 若為偶數則為「偶數會員」，若為奇數則為「奇數會員」
     */
    private void calcAccName() {
        /** 計算目前分鐘數為偶數或奇數 */
        String curr_status = (this.login_times % 2 == 0) ? "偶數會員" : "奇數會員";
        /** 將新的會員組別指派至Manager之屬性 */
        this.status = curr_status;
        /** 檢查該名會員是否已經在資料庫，若有則透過ManagerHelper物件，更新目前之組別狀態 */

        if(this.id != 0) mgh.updateStatus(this, curr_status);
    }
}