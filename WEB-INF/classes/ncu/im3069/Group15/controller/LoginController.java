package ncu.im3069.Group15.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import ncu.im3069.Group15.app.Login;
import ncu.im3069.Group15.app.LoginHelper;
import ncu.im3069.tools.JsonReader;


@WebServlet("/api/login.do")

public class LoginController  extends HttpServlet{
	 

	
	     
	     private static final long serialVersionUID = 1L;
	     
	     /** lh，LoginHelper之物件與Login相關之資料庫方法（Sigleton） */
	     private LoginHelper lh =  LoginHelper.getHelper();

	     public LoginController() {
	         super();
	     }
	  
	     public void doPost(HttpServletRequest request, HttpServletResponse response)
	         throws ServletException, IOException {
	         /** 透過JsonReader類別將Request之JSON格式資料解析並取回 */
	         JsonReader jsr = new JsonReader(request);
	         JSONObject jso = jsr.getObject();
	         
	         /** 若直接透過前端AJAX之data以key=value之字串方式進行傳遞參數，可以直接由此方法取回資料 */
	         String email = jso.getString("email");
	         String password = jso.getString("password");
	          System.out.println(email+password);
	        Login l = new Login(email, password);
	          
	        if(lh.getByData(l) == 1 ) {
	        	 System.out.println("Success,"+lh.getByData(l));
	        	 //JSONObject resp = new JSONObject();
	             //resp.put("status", "200");
	             //resp.put("message", "登入成功！");
	             //resp.put("response","You win");
	             String resp = "{\"status\": \'200\', \"message\": \'登入成功!!！\', \'response\': \'\'}";
	             jsr.response(resp, response);	
	        }
	        else {
	        	 System.out.println("Fail,"+lh.getByData(l));
	            /** 以字串組出JSON格式之資料 */
	            String resp = "{\"status\": \'400\', \"message\": \'Email或密碼錯誤!!！\', \'response\': \'\'}";
	            /** 透過JsonReader物件回傳到前端（以字串方式） */
	            jsr.response(resp, response);
	        }
	             
	     }
	           
	     }

	 
	    
	 

