package ncu.im3069.Group15.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;

import ncu.im3069.Group15.app.Member;
import ncu.im3069.Group15.app.MemberHelper;
import ncu.im3069.tools.JsonReader;

@WebServlet("/api/membercart.do")
public class MembercartController extends HttpServlet {
	
	  private static final long serialVersionUID = 1L;
	  private MemberHelper mh =  MemberHelper.getHelper();

	  
	  
	  public void doGet(HttpServletRequest request, HttpServletResponse response)
		        throws ServletException, IOException {
		        /** 透過JsonReader類別將Request之JSON格式資料解析並取回 */
		        JsonReader jsr = new JsonReader(request);
		        /** 若直接透過前端AJAX之data以key=value之字串方式進行傳遞參數，可以直接由此方法取回資料 */
		        String email = jsr.getParameter("email");
		        
		        /** 判斷該字串是否存在，若存在代表要取回個別會員之資料，否則代表要取回全部資料庫內會員之資料 */
		        
		            /** 透過MemberHelper物件之getAll()方法取回所有會員之資料，回傳之資料為JSONObject物件 */
		            JSONObject query = mh.getByEmail(email);
		            
		            /** 新建一個JSONObject用於將回傳之資料進行封裝 */
		            JSONObject resp = new JSONObject();
		            resp.put("status", "200");
		            resp.put("message", "所有會員資料取得成功");
		            resp.put("response", query);
		    
		            /** 透過JsonReader物件回傳到前端（以JSONObject方式） */
		            jsr.response(resp, response);
		        
		   
		        
		    }

		 
}

